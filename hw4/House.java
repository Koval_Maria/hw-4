package com.deveducation.www.hw4;

public class House {
    private String id;
    private int numberOfFlat;
    private int apartmentArea;
    private int floor;
    private int numberOfRooms;
    private String street;
    private String buildingType;
    private int lifetime;

    public House(String id, int numberOfFlat, int apartmentArea, int floor, int numberOfRooms, String street, String buildingType, int lifetime) {
        this.id = id;
        this.numberOfFlat = numberOfFlat;
        this.apartmentArea = apartmentArea;
        this.floor = floor;
        this.numberOfRooms = numberOfRooms;
        this.street = street;
        this.buildingType = buildingType;
        this.lifetime = lifetime;
    }


    public String setId(String id) {
        this.id = id;
        return id;
    }

    public int setNumberOfFlat(int numberOfFlat) {
        if (numberOfFlat < 1 || numberOfFlat > 2000) {
            throw new IllegalArgumentException("Number of flat can't be like this");
        }
        this.numberOfFlat = numberOfFlat;
        return numberOfFlat;
    }

    public int setApartmentArea(int apartmentArea) {
        if (apartmentArea < 3 || apartmentArea > 2000) {
            throw new IllegalArgumentException("Apartment area can't be like this");
        }
        this.apartmentArea = apartmentArea;
        return apartmentArea;
    }

    public int setFloor(int floor) {
        if (floor < 1 || floor > 165) {
            throw new IllegalArgumentException("Apartment floor can't be like this");
        }
        this.floor = floor;
        return floor;
    }

    public int setNumberOfRooms(int numberOfRooms) {
        if (numberOfRooms < 1 || numberOfRooms > 20) {
            throw new IllegalArgumentException("Quantity of rooms can't be like this");
        }
        this.numberOfRooms = numberOfRooms;
        return numberOfRooms;
    }

    public String setStreet(String street) {
        this.street = street;
        return street;
    }

    public String setBuildingType(String buildingType) {
        this.buildingType = buildingType;
        return buildingType;
    }

    public int setLifetime(int lifetime) {
        if (lifetime < 1 || lifetime > 3000) {
            throw new IllegalArgumentException("Lifetime of building can't be like this");
        }
        this.lifetime = lifetime;
        return lifetime;
    }

    public String getId() {
        return id;
    }

    public int getNumberOfFlat() {
        return numberOfFlat;
    }

    public int getApartmentArea() {
        return apartmentArea;
    }

    public int getFloor() {
        return floor;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public String getStreet() {
        return street;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public int getLifetime() {
        return lifetime;
    }

    @Override
    public String toString() {
        return "House{" +
                "id='" + id + '\'' +
                ", numberOfFlat=" + numberOfFlat +
                ", apartmentArea=" + apartmentArea +
                ", floor=" + floor +
                ", numberOfRooms=" + numberOfRooms +
                ", street='" + street + '\'' +
                ", buildingType='" + buildingType + '\'' +
                ", lifetime=" + lifetime +
                '}';
    }
}


