package com.deveducation.www.hw4;

import java.util.Arrays;

public class Abiturient extends Student {
    private String phoneNumber;
    private int[] grades;

    public Abiturient(String id, String surname, String name, String patronymic, String address, String phoneNumber, int[] grades) {
        super(id, surname, name, patronymic, address);
        this.phoneNumber = phoneNumber;
        this.grades = grades;
    }

    @Override
    public String setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return phoneNumber;
    }

    public int[] setGrades(int[] grades) {
        this.grades = grades;
        return grades;
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int[] getGrades() {
        return grades;
    }

    @Override
    public String toString() {
        return "Abiturient{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", grades=" + Arrays.toString(grades) +
                ", id='" + id + '\'' +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
