package com.deveducation.www.hw4;

public class Patient extends Student {
    private String medicalRecordNumber;
    private String diagnosis;
    private String phoneNumber;

    public Patient(String id, String surname, String name, String patronymic, String address, String phoneNumber, String medicalRecordNumber, String diagnosis) {
        super(id, surname, name, patronymic, address);
        this.medicalRecordNumber = medicalRecordNumber;
        this.diagnosis = diagnosis;
        this.phoneNumber = phoneNumber;
    }

    public String setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
        return medicalRecordNumber;
    }

    public String setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
        return diagnosis;
    }

    @Override
    public String setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return phoneNumber;
    }

    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
        return "id - " + id + ", surname - " + surname + ", name - " + name + ", patronymic - " + patronymic + ", address - " + address + ", phone number - " + phoneNumber + ", medical record number - " + medicalRecordNumber + ", diagnosis - " + diagnosis + ".";

    }
}
