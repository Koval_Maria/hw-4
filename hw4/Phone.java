package com.deveducation.www.hw4;

import java.security.SecureRandom;

public class Phone extends Student{

    private String numberOfCreditCard;
    private int debit;
    private int credit;
    private long talkTime;
    public Phone(String id, String surname, String name, String patronymic, String address, String numberOfCreditCard, int debit, int credit, long talkTime) {
        super(id, surname, name, patronymic, address);
        this.numberOfCreditCard = numberOfCreditCard;
        this.debit = debit;
        this.credit = credit;
        this.talkTime = talkTime;
    }

    public String setNumberOfCreditCard(String numberOfCreditCard) {
        this.numberOfCreditCard = numberOfCreditCard;
        return numberOfCreditCard;
    }

    public int setDebit(int debit) {
        if (debit < 1) {
            throw new IllegalArgumentException("Please top up your mobile account");
        }
        this.debit = debit;
        return debit;
    }

    public int setCredit(int credit) {
        this.credit = credit;
        return credit;
    }

    public long setTalkTime(long talkTime) {
        this.talkTime = talkTime;
        return talkTime;
    }

    public String getNumberOfCreditCard() {
        return numberOfCreditCard;
    }

    public int getDebit() {
        return debit;
    }

    public int getCredit() {
        return credit;
    }

    public long getTalkTime() {
        return talkTime;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "numberOfCreditCard='" + numberOfCreditCard + '\'' +
                ", debit=" + debit +
                ", credit=" + credit +
                ", talkTime=" + talkTime +
                ", id='" + id + '\'' +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}