package com.deveducation.www.hw4;

public class Book {
    private String id;
    private String title;
    private String authors;
    private String publishingHouse;
    private int yearOfPublishing;
    private int numberOfPages;
    private double price;
    private String cover;

    public Book(String id, String title, String authors, String publishingHouse, int yearOfPublishing, int numberOfPages, double price, String cover) {
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.publishingHouse = publishingHouse;
        this.yearOfPublishing = yearOfPublishing;
        this.numberOfPages = numberOfPages;
        this.price = price;
        this.cover = cover;
    }

    public String setId(String id) {
        this.id = id;
        return id;
    }

    public String setTitle(String title) {
        this.title = title;
        return title;
    }

    public String setAuthors(String authors) {
        this.authors = authors;
        return authors;
    }

    public String setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
        return publishingHouse;
    }

    public int setYearOfPublishing(int yearOfPublishing) {
        this.yearOfPublishing = yearOfPublishing;
        return yearOfPublishing;
    }

    public int setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
        return numberOfPages;
    }

    public double setPrice(double price) {
        this.price = price;
        return price;
    }

    public String setCover(String cover) {
        this.cover = cover;
        return cover;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthors() {
        return authors;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public int getYearOfPublishing() {
        return yearOfPublishing;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public double getPrice() {
        return price;
    }

    public String getCover() {
        return cover;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", authors='" + authors + '\'' +
                ", publishingHouse='" + publishingHouse + '\'' +
                ", yearOfPublishing=" + yearOfPublishing +
                ", numberOfPages=" + numberOfPages +
                ", price=" + price +
                ", cover='" + cover + '\'' +
                '}';
    }
}
