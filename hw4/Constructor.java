package com.deveducation.www.hw4;


public class Constructor {

    public static void main(String[] args) {


        String[] idArray = {"id0001", "id0002", "id0003", "id0004", "id0005", "id0006", "id0007", "id0008", "id0009", "id0010"};
        String[] surnameArray = {"Adamson", "Andrews", "Barnes", "Brickman", "Dean", "Foster", "Garrison", "Hancock", "Holmes", "Kelly"};
        String[] nameArray = {"Alexander", "Anton", "Boris", "Valeriy", "Vitaly", "Gennady", "Gleb", "Dmitry", "Egor", "Ivan"};
        String[] patronymicArray = {"Yuryevich", "Aleksandrovich", "Anatolyevich", "Vyacheslavovich", "Valeryevich", "Victorovich", "Mikhaylovich", "Vasilyevich", "Vladimirovich", "Vyacheslavovich"};
        int[] dateOfBirthArray = {2000, 1999, 1998, 2002, 2001, 2002, 2001, 1999, 2001, 1997};
        String[] addressArray = {"Kharkov, ul. Tsvetochnaya d.5 kv.57", "Kharkov, prosp. Stroiteley d. 30-б kv. 125", "Kharkov, Microraion Zelenyi d. 15 kv. 97", "Kharkov, 3th Microraion d. 48-a kv. 100", "Kharkov, ul. Tsvetochnaya d.8 kv.100", "Kharkov, prosp. Stroiteley d. 1-3 kv. 99", "Kharkov, Microraion Zelenyi d. 4 kv. 24", "Kharkov, 3th Microraion d. 7-a kv. 10", "Kharkov, Microraion Zelenyi d. 9 kv. 142", "Kharkov, 4th Microraion d. 4-a kv. 56"};
        String[] phoneNumberArray = {"380500621174", "380958019073", "380967040853", "380506275797", "380995097848", "380504964702", "380500659280", "380501849726", "380632690087", "380507395310"};
        String[] facultyArray = {"Faculty of Germanic Philology", "Faculty of Oriental Studies", "Faculty of Translation Studies", "Faculty of Romance Philology of Translation", "Faculty of Slavic Philology", "Faculty of Economics and Law", "Faculty of Germanic Philology", "Faculty of Oriental Studies", "Faculty of Translation Studies", "Faculty of Romance Philology of Translation"};
        int[] courseArray = {1, 2, 3, 1, 1, 1, 1, 2, 1, 4};
        String[] groupArray = {"FGP-1", "FOS-2", "FTS-3", "FRPT-1", "FSP-1", "FEL-1,", "FGP-1", "FOS-2", "FTS-1", "FRPT-4"};
        String[] bankAccountNumberArray = {"UANN3808840000026001234567890", "UANN2108840000026001234567891", "UANN4908840000026001234567890", "UANN9928840000026001234567890", "UANN2768840000026001234567890", "UANN1108840000026001234567890", "UANN8548840000026001234567890", "UANN5658840000026001234567890", "UANN4418840000026001234567890", "UANN7028840000026001234567890"};
        String[] numberOfCreditCardArray = {"4274 5536 0842 7505", "4554 0741 0761 9048", "4048 8466 8551 7163", "4031 5970 9672 0459", "4035 5970 9672 0459", "4598 7104 7318 5969", "4947 6515 1559 6573", "4171 0264 3384 0204", "4072 9462 0681 5374", "4329 2883 0095 6098"};
        String[] medicalRecordNumberArray = {"817892", "217893", "527892", "921892", "107891", "747890", "557892", "617892", "501892", "107892"};
        String[] diagnosisArray = {"cancer", "diabetes", "anaemia", "cancer", "meningitis", "cancer", "autism", "hepatitis", "jaundice", "prostatitis"};
        String[] titleArray = {"1984", "Shantaram", "Master and Margarita", "Three comrades", "Flowers for Algernon", "Catcher in the rye", "The little Prince", "The Picture of Dorian Grey", "Dandelion Wine", "Atlas Shrugged"};
        String[] authorsArray = {"George Orwell", "Gregory David Roberts", "Michael Bulgakov", "Erich Maria Remarque", "Daniel Keyes", "Jerome D. Salinger", "Antoine de Saint-Exupery", "Oscar Wilde", "Ray Bradbury", "Ayn Rand"};
        String[] publishingHouseArray = {"EKSMO", "AST", "ROSMEN", "EKSMO", "AST", "ROSMEN", "EKSMO", "AST", "ROSMEN", "ROSMEN"};
        int[] numberOfPagesArray = {298, 392, 136, 300, 401, 281, 248, 300, 243, 182};
        double[] priceArray = {21.1, 14.5, 11.3, 30.99, 21.1, 14.5, 11.3, 30.99, 11.01, 9.99};
        String[] coverArray = {"soft", "hard", "soft", "hard", "soft", "hard", "soft", "hard", "soft", "hard"};
        int[] numberOfFlatArray = {101, 1, 323, 96, 32, 19, 496, 11, 92, 12};
        int[] apartmentAreaArray = {34, 1100, 87, 47, 91, 56, 77, 100, 41, 81};
        int[] floorArray = {1, 3, 6, 3, 2, 7, 2, 81, 17, 9};
        int[] numberOfRoomsArray = {1, 3, 6, 3, 2, 7, 2, 2, 1, 4};
        String[] streetArray = {"ul. Tsvetochnaya", "prosp. Stroiteley", "ul. Microraion Zelenyi", "ul. 3th Microraion", "ul. Tsvetochnaya", "prosp. Stroiteley", "ul. Microraion Zelenyi", "ul. 3th Microraion", "ul. Microraion Zelenyi", "ul. 4th Microraion"};
        String[] buildingTypeArray = {"dwelling house", "industrial building", "dwelling house", "industrial building", "dwelling house", "industrial building", "dwelling house", "industrial building", "dwelling house", "industrial building"};
        int[] lifetimeArray = {100, 65, 50, 200, 199, 100, 65, 50, 200, 199};


        Student[] arrayStudentOfFacultyOfOrientalStudies = new Student[10];

        for (int i = 0; i < arrayStudentOfFacultyOfOrientalStudies.length; i++) {

            if (facultyArray[i].equals("Faculty of Oriental Studies")) {
                arrayStudentOfFacultyOfOrientalStudies[i] = new Student(idArray[i], surnameArray[i], nameArray[i], patronymicArray[i], dateOfBirthArray[i], addressArray[i], phoneNumberArray[i], facultyArray[i], courseArray[i], groupArray[i]);
            } else {
                continue;
            }
            System.out.println("The students of Faculty of Oriental Studies: " + arrayStudentOfFacultyOfOrientalStudies[i]);
        }
        System.out.println("----------------------------------------");


        Student[] arrayStudentBirthAfter2000 = new Student[10];
        for (int i = 0; i < arrayStudentOfFacultyOfOrientalStudies.length; i++) {
            if (dateOfBirthArray[i] > 2000) {
                arrayStudentBirthAfter2000[i] = new Student(idArray[i], surnameArray[i], nameArray[i], patronymicArray[i], dateOfBirthArray[i], addressArray[i], phoneNumberArray[i], facultyArray[i], courseArray[i], groupArray[i]);
            } else {
                continue;
            }
            System.out.println("The students born after 2000: " + arrayStudentBirthAfter2000[i]);
        }
        System.out.println("----------------------------------------");


        Student[] arrayStudentOfGroup = new Student[10];
        for (int i = 0; i < arrayStudentOfGroup.length; i++) {
            if (groupArray[i].equals("FGP-1")) {
                arrayStudentOfGroup[i] = new Student(idArray[i], surnameArray[i], nameArray[i], patronymicArray[i], dateOfBirthArray[i], addressArray[i], phoneNumberArray[i], facultyArray[i], courseArray[i], groupArray[i]);
            } else {
                continue;
            }
            System.out.println("The students of group FGP-1: " + arrayStudentOfGroup[i]);
        }
        System.out.println("----------------------------------------");

        Customer[] arrayCustomerOfNumberCard = new Customer[10];

        for (int i = 0; i < arrayCustomerOfNumberCard.length; i++) {

            if (numberOfCreditCardArray[i].equals("4031 5970 9672 0459")) {
                arrayCustomerOfNumberCard[i] = new Customer(idArray[i], surnameArray[i], nameArray[i], patronymicArray[i], addressArray[i], numberOfCreditCardArray[i], bankAccountNumberArray[i]);
            } else {
                continue;
            }
            System.out.println("The customer with number of credit card 4031 5970 9672 0459: " + arrayCustomerOfNumberCard[i]);
        }
        System.out.println("----------------------------------------");

        Patient[] arrayPatientWithCancer = new Patient[10];

        for (int i = 0; i < arrayPatientWithCancer.length; i++) {

            if (diagnosisArray[i].equals("cancer")) {
                arrayPatientWithCancer[i] = new Patient(idArray[i], surnameArray[i], nameArray[i], patronymicArray[i], addressArray[i], phoneNumberArray[i], medicalRecordNumberArray[i], diagnosisArray[i]);
            } else {
                continue;
            }
            System.out.println("The patients with cancer: " + arrayPatientWithCancer[i]);
        }
        System.out.println("----------------------------------------");


        Patient[] arrayPatientWithConcreteCardNumber = new Patient[10];

        for (int i = 0; i < arrayPatientWithConcreteCardNumber.length; i++) {

            if (medicalRecordNumberArray[i].equals("527892")) {
                arrayPatientWithConcreteCardNumber[i] = new Patient(idArray[i], surnameArray[i], nameArray[i], patronymicArray[i], addressArray[i], phoneNumberArray[i], medicalRecordNumberArray[i], diagnosisArray[i]);
            } else {
                continue;
            }
            System.out.println("The patients with card № 527892: " + arrayPatientWithConcreteCardNumber[i]);
        }
        System.out.println("----------------------------------------");

       Book[] booksOfMichaelBulgakov = new Book[10];
        for (int i = 0; i < booksOfMichaelBulgakov.length; i++) {

            if (authorsArray[i].equals("Michael Bulgakov")) {
                booksOfMichaelBulgakov[i] = new Book(idArray[i], titleArray[i], authorsArray[i], publishingHouseArray[i], dateOfBirthArray[i], numberOfPagesArray[i], priceArray[i], coverArray[i]);
            } else {
                continue;
            }
            System.out.println("The books of Michael Bulgakov: " + booksOfMichaelBulgakov[i]);
        }
        System.out.println("----------------------------------------");

        Book[] booksOfEKSMO = new Book[10];
        for (int i = 0; i < booksOfEKSMO.length; i++) {

            if (publishingHouseArray[i].equals("EKSMO")) {
                booksOfEKSMO[i] = new Book(idArray[i], titleArray[i], authorsArray[i], publishingHouseArray[i], dateOfBirthArray[i], numberOfPagesArray[i], priceArray[i], coverArray[i]);
            } else {
                continue;
            }
            System.out.println("The books from publishing house EKSMO: " + booksOfEKSMO[i]);
        }
        System.out.println("----------------------------------------");

        Book[] booksReleasedAfter2000 = new Book[10];
        for (int i = 0; i < booksReleasedAfter2000.length; i++) {

            if (dateOfBirthArray[i] > 2000) {
                booksReleasedAfter2000[i] = new Book(idArray[i], titleArray[i], authorsArray[i], publishingHouseArray[i], dateOfBirthArray[i], numberOfPagesArray[i], priceArray[i], coverArray[i]);
            } else {
                continue;
            }
            System.out.println("The books released after 2000: " + booksReleasedAfter2000[i]);
        }
        System.out.println("----------------------------------------");



        House[] arrayHouseSpecificNumberOfRooms = new House[10];

        for (int i = 0; i < arrayHouseSpecificNumberOfRooms.length; i++) {

            if (numberOfRoomsArray[i] == 2) {
                arrayHouseSpecificNumberOfRooms[i] = new House(idArray[i], numberOfFlatArray[i], apartmentAreaArray[i], floorArray[i], numberOfRoomsArray[i], streetArray[i], buildingTypeArray[i], lifetimeArray[i]);
            } else {
                continue;
            }
            System.out.println("The flat with 2 rooms: " + arrayHouseSpecificNumberOfRooms[i]);
        }
        System.out.println("----------------------------------------");


        House[] arrayHouseSpecificNumberOfRoomsAndFloor = new House[10];

        for (int i = 0; i < arrayHouseSpecificNumberOfRoomsAndFloor.length; i++) {

            if (numberOfRoomsArray[i] == 2 && floorArray[i] > 1 && floorArray[i] < 10) {
                arrayHouseSpecificNumberOfRoomsAndFloor[i] = new House(idArray[i], numberOfFlatArray[i], apartmentAreaArray[i], floorArray[i], numberOfRoomsArray[i], streetArray[i], buildingTypeArray[i], lifetimeArray[i]);
            } else {
                continue;
            }
            System.out.println("The flat with 2 rooms and floor > 1 and < 10: " + arrayHouseSpecificNumberOfRoomsAndFloor[i]);
        }
        System.out.println("----------------------------------------");

        House[] arrayHouseWithAreaMoreThan50 = new House[10];

        for (int i = 0; i < arrayHouseWithAreaMoreThan50.length; i++) {

            if (apartmentAreaArray[i] > 50) {
                arrayHouseWithAreaMoreThan50[i] = new House(idArray[i], numberOfFlatArray[i], apartmentAreaArray[i], floorArray[i], numberOfRoomsArray[i], streetArray[i], buildingTypeArray[i], lifetimeArray[i]);
            } else {
                continue;
            }
            System.out.println("The flat with more than 50 m2: " + arrayHouseWithAreaMoreThan50[i]);
        }
        System.out.println("----------------------------------------");

    }

}





