package com.deveducation.www.hw4;

public class Student {
    protected String id;
    public String surname;
    protected String name;
    protected String patronymic;
    private int dateOfBirth;
    protected String address;
    private String phoneNumber;
    private String faculty;
    private int course;
    private String group;


   public Student (String id, String surname, String name, String patronymic, int dateOfBirth, String address, String phoneNumber, String faculty, int course, String group) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    public Student (String id, String surname, String name, String patronymic, String address) { //этот конструктор нужен для наследования
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
    }




    public String setId(String id) {
        this.id = id;
        return id;
    }

    public String setSurname(String surname) {
        this.surname = surname;
        return surname;
    }

    public String setName(String name) {
        this.name = name;
        return name;
    }

    public String setPatronymic(String patronymic) {
        this.patronymic = patronymic;
        return patronymic;
    }

    public int setDateOfBirth(int dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return dateOfBirth;
    }

    public String setAddress(String address) {
        this.address = address;
        return address;
    }

    public String setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return phoneNumber;
    }

    public String setFaculty(String faculty) {
        this.faculty = faculty;
        return faculty;
    }

    public int setCourse(int course) {
        this.course = course;
        return course;
    }

    public String setGroup(String group) {
        this.group = group;
        return group;
    }

    public String getId() {
        return id;
    }
    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public int getDateOfBirth() {
        return dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getFaculty() {
        return faculty;
    }

    public int getCourse() {
        return course;
    }

    public String getGroup() {
        return group;
    }

    @Override
    public String toString() {
        return "id - " + id + ", surname - " + surname + ", name - " + name + ", patronymic - " + patronymic + ", date of birth - " + dateOfBirth + ", address - " + address + ", phone number - " + phoneNumber + ", faculty - " + faculty + ", course - " + course + ", group - " + group + ".";
    }

}

